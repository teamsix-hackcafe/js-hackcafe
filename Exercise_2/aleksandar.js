function addClassToList(list, className) {
    var i = list.length - 1;
    for (i; i >= 0; i--) {
        list[i].classList.add(className);
    }
}

function fillObject(list, type, value) {
    var i = list.length - 1;
    var result = [];
    for (i; i >= 0; i--) {
        var obj = {};
        if (i%2 === 0) {
            obj[type[i].innerText] = value[i].innerText;
            obj[type[i+1].innerText] = value[i+1].innerText;
            result.push(obj);
        }
        else {
            obj[type[i+9].innerText] = value[i+9].innerText;
            obj[type[i+10].innerText] = value[i+10].innerText;
            result.push(obj);
        }
    }

    return result;
}


(function() {
    var evenRows = document.querySelectorAll("#table tr:nth-child(even)"),
        oddRows = document.querySelectorAll("#table tr:nth-child(odd)"),
        keysElement = document.querySelectorAll("#table tr:nth-child(even) > td:nth-child(odd)"),
        keysValues = document.querySelectorAll("#table tr:nth-child(even) > td:nth-child(even)"),
        objects = [{
            id: 1,
            name: "Pesho"
        }];

    addClassToList(evenRows, "even");
    addClassToList(oddRows, "odd");
    objects.push(fillObject(evenRows, keysElement, keysValues));
    console.log(objects);
})();