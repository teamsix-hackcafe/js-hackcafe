// Task One
function max (a, b) {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}
// console.log(max(1, 2));

// Task Two
function maxOfThree (a, b, c) {
    return Math.max(a, b, c);
}
// console.log(maxOfThree(1, 3, 2));

// Task 3
function isVowel (character) {
    if (character.length === 1) {
        return 'aeiou'.indexOf(character) >= 0 ? true : false;
    }
    else
        return false;
}
// console.log(isVowel('a'));

// Task 4
function transform (string) {
    var result = [];
    string.split('').forEach(function (character) {
        if (!isVowel(character) && character !== ' ') {
            character = character +'o' + character;
        }
        result.push(character);
    });
    return result.join('');
}
// console.log(transform('this is fun'));

// Task 5
function sum (array) {
    return array.reduce(function (a, b) {
        return a + b;
    });
}
// console.log(sum([1, 2, 3, 4, 5]));

function multiply (array) {
    return array.reduce(function (a, b) {
        return a * b;
    });
}
// console.log(multiply([1, 2, 3, 4, 5]));

// Task 6
function reverse (string) {
    return string.split('').reverse().join('');
}
// console.log(reverse('jag testar'));


// Task 7
var lexicon = {
    "merry":"god",
    "christmas":"jul",
    "and":"och",
    "happy":"gott",
    "new":"nytt",
    "year":"år"
};

function translate (string) {
    var result = '';
    string.split(' ').forEach(function (word) {
        result += (lexicon[word] || word) + ' ';
    });
    return result;
}
// console.log(translate('merry christmas and happy new year nigga'));

// Task 8
function findLongestWord (array) {
    var result = array.reduce(function (a, b) {
        return (a.length > b.length) ? a : b;
    });
    return result.length;
}
// console.log(findLongestWord(['god', 'damn', 'it']));

// Task 9
function filterLongWords (array, number) {
    var result = [];
    array.forEach(function (word) {
        if(word.length > number) result.push(word);
    });
    return result;
}
// console.log(filterLongWords(['god', 'damn', 'it'], 2));

// Task 10
function charFreq (string) {
    var frequency = {};
    string.split('').forEach(function (character) {
        if (!frequency[character]) frequency[character] = 0;
        frequency[character]++;
    });
    return frequency;
}
// console.log(charFreq('abbabcbdbabdbsssseadbabababcbcbab'));

// Task 11
function checkValue (number) {
    if ((Math.ceil(Math.log((number) + 1) / Math.LN10)) >= 7) {
        return 'Result ' + number + 'dollars ($$$)';
    }
    else
        return 'Result ' + number + 'leva';
}
// console.log(checkValue(1000000));


// Task 12
function mixUp (a, b) {
    if (a.length >= 2 && b.length >= 2) {
        return (b.substr(0, 2) + a.slice(2)) + ' ' + (a.substr(0, 2) + b.slice(2));
    }
}
// console.log(mixUp('mix', 'pod'));

// Task 13
function fixStart (string) {
    if (string.length >= 1) {
        var result = [];
        string.split('').forEach(function (character) {
            if (string.charAt(0) === character && result.indexOf(string.charAt(0)) === 0) {
                result.push('*');
            }
            else
                result.push(character);
        });
        return result.join('');
    }
}
// console.log(fixStart('babble'));

// Task 14
function verbing (string) {
    if (string.length >= 3) {
        string = (string.slice(string.length-3) === 'ing') ? string + 'ly' : string + 'ing';
    }
    return string;
}
// console.log(verbing('verb'));

// Task 15
function notBad (string) {
    if (string.indexOf('not') < string.indexOf('bad') && string.indexOf('bad') < (string.indexOf('not') + 10)) {
        string = string.slice(0, string.indexOf('not')) + 'good' + string.slice(string.indexOf('bad') + 3);
    }
    return string;
}
// console.log(notBad('This dinner is not that bad!'));

// Task 16
function removeRandomItem (array, getInfo) {
    getInfo = typeof getInfo === 'boolean' ? getInfo : false;
    if (array.length > 0) {
        var rand = Math.floor(Math.random() * array.length);
        var removed = array.splice(rand, 1).toString();
        return getInfo ? '"' + removed + '" leaving [' + array + ']' : removed;
    }
    else
        return null;
}
// console.log(removeRandomItem(["foo", "bar", "baz"], true));

// Task 17
function randomizeOrder (array) {
    var result = [];
    while(array.length !== 0) {
        result.push(removeRandomItem(array));
    }
    return result;
}
// console.log(randomizeOrder(["a", "b", "c", "d", "e", "f"]));