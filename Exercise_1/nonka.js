//Task 2
function maxOfThree(a, b, c) {
    return Math.max(a, b, c);
}

console.log('Task 2:');
console.log(maxOfThree(1, 2, 3));

//Task 3
function vowel(char1) {
    if (char1.length == 1) {
        if (char1.match(/[aeiouAEIOU]/)) {
            return true;
        }
        else {
            return false;
        }
    }
}

console.log('Task 3:');
console.log(vowel('b'));

//Task 4
function translate(string) {
    var result = '';
    for (i = 0; i < string.length; i++) {
        if(!vowel(string[i]) && string[i] != ' ') {
            result += string[i] + 'o' + string[i];
        }
        else {
            result += string[i];
        }
    }
    return result;
}

console.log('Task 4:');
console.log(translate('this is fun'));

//Task 5
function sum(array) {
    var result = 0;
    for (i = 0; i < array.length; i++) {
        array[i] = parseInt(array[i]);
        result += array[i];
    }
    return result;
}

function multiply(array) {
    var result = 1;
    for (i = 0; i < array.length; i++) {
        array[i] = parseInt(array[i]);
        result *=  array[i];
    }
    return result;
}

var arr = [1, 2, 3,];

console.log('Task 5:');
console.log('Sum ' + sum(arr) + ', Multiply ' + multiply(arr));

//Task 6

function reverse(s) {
    return s.charAt(s.length-1) + s.substring(1,s.length-1) + s.charAt(0);
}

console.log('Task 6:');
console.log(reverse('acccccd'));

//Task 7
var obj = {
    "merry":"god",
    "christmas":"jul",
    "and":"och",
    "happy":"gott",
    "new":"nytt",
    "year":"år"
};

var swedish = "";
function translateCard(string) {
        for (var word in obj) {
            if (obj.hasOwnProperty(word)) {
                var allText = string.split(/\s/);

                for (var i = 0; i <= allText.length; i++) {
                    if (allText[i] === word) {
                    swedish += obj[word] + " ";
                    }
            }
        }
    }
    swedish = swedish.trim();
    swedish = swedish += "!";
    return swedish[0].toUpperCase() + swedish.slice(1);
}

console.log('Task 7:');
console.log(translateCard("merry christmas and happy new year"));

//Task 8

function findLongWords(string) {
    var longest = 0;
    //string = string.split(/\s/);
    for ( var j = 0; j < string.length; j++) {
        if (string[j].length > longest) {
            longest = string[j].length;
        }
    }
  return longest;
}
arrWords = "Lorem ipsum dolor sit amet, consectetur adipisicing elit.";
arrWords = arrWords.split(/\s/);

console.log('Task 8:');
console.log(findLongWords(arrWords));

//Task 9

function filterLongWords(words, i) {
    var result = [];
    words.forEach(function(word) {
        if (word.length > i) {
            result.push(word);
        }
    })
    return result;
}

console.log('Task 9:');
console.log(filterLongWords(arrWords, 10));

//Task 10

var charFreq = function(string) {
    var list = {};
    string.split('').forEach(function(letter) {
        if (letter in list)
        list[letter] += +1;
        else
        list[letter] = 1;
    })
    return list;
}

console.log('Task 10:');
console.log(charFreq('abbabcbdbabdbdbabababcbcbab'));

//Task 11

function chekValue(number) {
    parseInt(number);
    if(number >= 1000000 ) {
        return number + ' dollars ($$$)';
    }
    else {
        return number + ' leva';
    }
}

console.log('Task 11:');
console.log(chekValue(1000000));

//Task 12

function mixUp(str1, str2) {
    if(str1.length > 2 && str2.length) {
        return str2.substring(0,2) + str1.slice(2)+' '+ str1.substring(0,2) + str2.slice(2);
    }
}

console.log('Task 12:');
console.log(mixUp('mix', 'pod'));

//Task 13

function fixStart(string) {
    first = string.charAt(0);
    index = string.indexOf(first, 1);

    string = string.split('');
    string[index] = '*';
    return string.join('');
}

console.log('Task 13:');
console.log(fixStart('babble'));

//Task 14

function verbing(string) {
    if(string.length <= 2) {
        return string;
    }
    if(string.slice(-3) == 'ing') {
        return string + 'ly';
    }
    else {
        return string + 'ing';
    }

}

console.log('Task 14:');
console.log(verbing('verb'));

//Task 15

function notBad(string) {
    var notIndex = string.indexOf('not');
    var badIndex = string.indexOf('bad');
    if (notIndex === -1 || badIndex === -1 || (badIndex < notIndex)) return string;
    return string.slice(0, notIndex) + 'good' + string.slice(badIndex + 3);
}

console.log('Task 15:');
console.log(notBad('This dinner is not that bad!'));

//Task 16

function removeRandomItem(array, bool) {
    if(array.length == 0){
        return 'result null leaving []';
    }
    random = Math.floor(Math.random() * array.length);
    remove = array.splice(random, 1);
    if (bool === true) {
        array.push(remove.toString());
        array.sort();
        return array;
    };
    return 'result ' + remove + ' leaving [' + array + ']';
}

console.log('Task 16:');
console.log(removeRandomItem(["foo", "bar", "baz"]));

//Task 17

function randomizeOrder(array) {
    removeRandomItem(array, true);
    console.log('Task 17:');
    console.log(array);
}

randomizeOrder(["foo", "bar", "baz"]);