var data = [],
    table = document.getElementById('table-wrap');

(function() {
    var ajax = new XMLHttpRequest();

    ajax.open('GET', '../json.json', true);

    ajax.onreadystatechange = function() {
        if (ajax.readyState === 4 && ajax.status === 200) {
            data = JSON.parse(ajax.responseText);
            tableCreator(data, 'table-style', 'rightAscension', table);
        }
    };

    ajax.send(null);
})();

/**
 * Creates HTML table.
 *
 * @param  {Array}  data       The data to work with.
 * @param  {String} className  Optional: Give class name. Default empty.
 * @param  {String} columnName Optional: Sort data by column.
 *                                       If the first character is "-" order it descending.
 * @param  {DOM Object} place  Optional: Where to create the table. Default returns String.
 * @return {String}            If no "place" is given returns the table HTML in String.
 */
function tableCreator(data, className, columnName, place) {
    if (columnName) {
        sortColumn(data, columnName);
    }

    var table = '<table' + (className ? ' class="' + className + '"' : '') + '>';
    table += createTableHead(data) + createTableBody(data) + '</table>';

    if (place) {
        place.innerHTML = table;
    }
    else {
        return table;
    }
}

/**
 * Sort HTML table by column.
 *
 * @param  {Array}  data    The data to work with.
 * @param  {String} key     The column name.
 * @param  {Number} orderBy Sort ascending/descending order.
 */
function sortColumn(data, key) {
    var hasDate = false,
        orderBy = 1;

    if (key.charAt(0) === '-') {
        orderBy = -1;
        key = key.slice(1);
    }

    data.sort(function(a, b) {
        switch(typeof a[key]) {
            case 'number':
                return (a[key] - b[key]) * orderBy;
            case 'string':
                var parts = a[key].split(' ');

                parts[1] = (parts[1].match(/\d+/) !== null) ? parts[1].match(/\d+/)[0] : 0;

                if (Date.parse(parts).toString() !== "NaN") {
                    hasDate = true;
                }

                return a[key].localeCompare(b[key]) * orderBy;
        }
    });

    // Sort again correctly for dates only.
    if (hasDate) {
        data.sort(function(a, b) {
            var partA = a[key].indexOf('NO DATA') > -1 ? 'Dec 31' : a[key].slice(0, a[key].length - 2),
                partB = b[key].indexOf('NO DATA') > -1 ? 'Dec 31' : b[key].slice(0, b[key].length - 2);

            return (Date.parse(partA) - Date.parse(partB)) * orderBy;
        });
    }
}

/**
 * Creates HTML Table Head.
 *
 * @param  {Array}  data The data to work with.
 * @return {String}      Returns the table head in String.
 */
function createTableHead(data) {
    var thead = '<thead>',
        list = [];

    data.forEach(function(obj) {
        for(var key in obj) {
            if (list.indexOf(key) === -1) {
                list.push(key);
            }
        }
    });

    return thead.concat(rowCreator(list, 'th'), '</thead>');
}

/**
 * Creates the body of a HTML Table.
 *
 * @param  {Array}  data The data to work with.
 * @return {String}      Returns the table body in String.
 */
function createTableBody(data) {
    var tbody = '<tbody>',
        list = [];

    data.forEach(function(obj) {
        for(var key in obj) {
            if (typeof obj[key] === 'object' && obj[key] !== null) {
                // Must give Array to tableCreator()!
                list.push(tableCreator([obj[key]]));
            }
            else if (obj[key] === null) {
                list.push('null');
            }
            else {
                list.push(obj[key]);
            }
        }
        tbody += rowCreator(list, 'td');
        // Empty array after 1 row added.
        list.splice(0, list.length);
    });

    return tbody.concat('</tbody>');
}

/**
 * Creates single row of HTML table.
 *
 * @param  {Array}  list    List of data for every column.
 * @param  {String} wrapTag How to wrap every single data.
 * @return {String}         Returns table row in String.
 */
function rowCreator(list, wrapTag) {
    var openTag = '<' + wrapTag + '>',
        closeTag = '</' + wrapTag + '>';

    return '<tr>' + openTag + list.join(closeTag + openTag) + closeTag + '</tr>';
}