var data;
var table = document.getElementById("table");

function ajaxGet() {
    var hr = new XMLHttpRequest();
    hr.open("GET", "../json.json", true);
    hr.onreadystatechange = function() {
        if (hr.readyState == 4 && hr.status == 200) {
            data =  JSON.parse(hr.responseText);
            render("name");
        }
    };
    hr.send();
}

ajaxGet();

function render(sortBy) {
    data = data.sort(sortTable(sortBy, 1));
    var header = createHeader(data[0]);
    table.innerHTML = "";
    table.innerHTML = header + makeTable(data);
    return true;
}

function makeTable(data, content, type) {
    content = content || "";
    type = type || "tr";

    for(var i in data) {
        if (typeof data[i] === "object" && data[i] !== null) {
            if (type === "td") {
                content += infieldTable(data[i]);
            }
            else {
                content = addRow(data[i], content);

                console.log();
            }
        }
        else {
            content += addField(data[i]);
        }
    }
    return content;
}

function infieldTable(data) {
    var content = "<td><table>"+ createHeader(data);
    content = makeTable(data, content, "td");
    content += "</table></td>";
    return content;
}

function createHeader(data) {
    var header = Object.getOwnPropertyNames(data).join('</th><th>');
    header = '<thead><tr><th>' + header + '</th></tr></thead>';
    return header;

}

function addField(data) {
    return "<td>"+ data + "</td>";
}

function addRow(data, content) {
    return "<tr>"+ makeTable(data, content, "td") + "</tr>";
}




/***
 * Comment Penalty -5 points
 */
function sortTable(property, sortOrder) {
    sortOrder = sortOrder || 1;

    /***
     * Missing ; -5 points
     */
    return function(a, b) {
        if (a[property] < b[property]) {
            return -1 * sortOrder;
        }
        if (a[property] > b[property]) {
            return 1 * sortOrder;
        }
        return 0;
    };
}

/***
 * + function to get te json
 * + function to build the HTML table from the DATA object
 * + function to sort the table by given column
 * + to be able to call a function that will sort and redraw the table
 */